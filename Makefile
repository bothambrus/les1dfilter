init:
	pip3 install -r requirements.txt

reinit:
	pip3 install --upgrade --force-reinstall  -r requirements.txt

checkstyle:
	black --check les1dfilter 
	black --check tests
	black --check examples
	isort --profile black --diff les1dfilter 
	isort --profile black --diff tests
	isort --profile black --diff examples

style:
	isort --profile black les1dfilter
	black les1dfilter 
	black tests
	black examples

test:
	rm -rf tests/tmp_testing/
	pytest --junitxml=junit_report.xml --cov=les1dfilter --cov-report=html tests
	coverage xml
	mkdir -p dist 
	rm -rf dist/htmlcov
	mv coverage.xml dist/
	mv htmlcov dist/
	echo open dist/htmlcov/index.html to review code coverage
	python .cicd_scripts/test_coverage.py 90 dist/coverage.xml



.PHONY: init test

