#################################################################################
#   LES1DFilter                                                                 #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import numpy as np
from .context import les1dfilter
from les1dfilter.box import boxFilterAtPoint, boxFilter


def test_box_filter_at_a_point_unit():
    """
    Test applying box filter at a single point on constant function.
    """

    x = [0, 2, 3, 4]
    y = [1, 1, 1, 1]

    print(x)
    print(y)

    #
    # Apply filter at different points
    #
    ybar1 = boxFilterAtPoint(0.0, x, y, 1.5)
    print(ybar1)
    ybar2 = boxFilterAtPoint(1.0, x, y, 1.5)
    print(ybar2)
    ybar3 = boxFilterAtPoint(2.0, x, y, 1.5)
    print(ybar3)
    ybar4 = boxFilterAtPoint(4.0, x, y, 1.5)
    print(ybar4)

    assert ybar1 == 1.0
    assert ybar2 == 1.0
    assert ybar3 == 1.0
    assert ybar4 == 1.0


def test_box_filter_at_a_point_linear():
    """
    Test applying box filter at a single point on a linear function.
    """

    x = [0, 2, 3, 4, 6, 7, 8]
    y = [0, 2, 3, 4, 6, 7, 8]

    print(x)
    print(y)

    #
    # Apply filter at different points
    #
    ybar1 = boxFilterAtPoint(0.0, x, y, 1.5)
    print(ybar1)
    ybar2 = boxFilterAtPoint(1.0, x, y, 1.5)
    print(ybar2)
    ybar3 = boxFilterAtPoint(2.0, x, y, 1.5)
    print(ybar3)
    ybar4 = boxFilterAtPoint(4.0, x, y, 1.5)
    print(ybar4)

    assert ybar1 == 0.375
    assert ybar2 == 1.0
    assert ybar3 == 2.0
    assert ybar4 == 4.0


def test_box_filter_unit():
    """
    Test applying box filter at all orignal points on a constant field.
    """

    x = [0, 2, 3, 4, 6, 7, 8]
    y = [1, 1, 1, 1, 1, 1, 1]

    #
    # Apply filter at different points
    #
    ybar = boxFilter(x, y, 1.5)
    print(ybar)

    assert all(ybar == [1, 1, 1, 1, 1, 1, 1])


def test_box_filter_linear():
    """
    Test applying box filter at all orignal points on a linear function.
    """

    x = [0, 2, 3, 4, 6, 7, 8]
    y = [0, 2, 3, 4, 6, 7, 8]

    #
    # Apply filter at different points
    #
    ybar = boxFilter(x, y, 1.5)
    print(ybar)

    assert all(ybar == [0.375, 2.0, 3.0, 4.0, 6.0, 7.0, 7.625])
