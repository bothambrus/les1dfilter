#################################################################################
#   LES1DFilter                                                                 #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import numpy as np
from .context import les1dfilter
from les1dfilter.field import getArrayInRange


def test_array_in_range():
    """
    Test slicing an array not on the grid points.
    """

    x = [0, 2, 3, 4, 6, 7, 8]
    y = [0, 1, 2, 3, 4, 5, 6]

    print(x)
    print(y)

    xarr1, yarr1 = getArrayInRange(x, y, 3, 6.5)
    print(xarr1, yarr1)

    xarr2, yarr2 = getArrayInRange(x, y, 2.5, 6.5)
    print(xarr2, yarr2)

    xarr3, yarr3 = getArrayInRange(x, y, 2.5, 6)
    print(xarr3, yarr3)

    assert len(yarr1) == 4
    assert len(yarr2) == 5
    assert len(yarr3) == 4

    assert yarr1[0] == 2.0
    assert yarr2[0] == 1.5
    assert yarr3[0] == 1.5

    assert yarr1[-1] == 4.5
    assert yarr2[-1] == 4.5
    assert yarr3[-1] == 4.0
