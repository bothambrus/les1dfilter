#################################################################################
#   LES1DFilter                                                                 #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import numpy as np
from .context import les1dfilter
from les1dfilter.integrate import trapezoidal, forwardIntegrate, backwardIntegrate


#
#  Test signal
#
# 2                x    x
#                  |    |
# 1           x    |    |
#             |    |    |
# 0 o---------o----o----o
#   0         2    3    4
#
#  Forward:     0 + 1   + 2 = 3
#  Backward:    2 + 2   + 2 = 6
#  Trapezoidal: 1 + 1.5 + 2 = 4.5
#
#


def test_trapezoidal():
    """
    Test trapezoidal integration.
    """

    x = [0, 2, 3, 4]
    y = [0, 1, 2, 2]

    print(x)
    print(y)

    inty = trapezoidal(x, y)
    print(inty)

    assert inty == 4.5


def test_forward_integration():
    """
    Test forward integration.
    """

    x = [0, 2, 3, 4]
    y = [0, 1, 2, 2]

    print(x)
    print(y)

    inty = forwardIntegrate(x, y)
    print(inty)

    assert inty == 3.0


def test_backward_integration():
    """
    Test backward integration.
    """

    x = [0, 2, 3, 4]
    y = [0, 1, 2, 2]

    print(x)
    print(y)

    inty = backwardIntegrate(x, y)
    print(inty)

    assert inty == 6.0
