#################################################################################
#   LES1DFilter                                                                 #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import numpy as np
from .context import les1dfilter
from les1dfilter.box import boxFilterAtPoint
from les1dfilter.interval import intervalFilterAtPoint
from les1dfilter.filterdensity import CFDFAtPoint, FDFAtPoint, JointFDFTable


def test_CFDF_at_a_point_unit_field():
    """
    Test evaluating the CFDF of the unit field.
    """

    x = [0, 2, 3, 4]
    y = [1, 1, 1, 1]
    delta = 1.5

    ydiscret = np.linspace(0.0, 2.0, 11)

    #
    # Evaluating CFDF at different points
    #
    CFDF1 = CFDFAtPoint(0.0, x, y, delta, ydiscret, boxFilterAtPoint)
    print(ydiscret)
    print(CFDF1)

    CFDF2 = CFDFAtPoint(2.1, x, y, delta, ydiscret, boxFilterAtPoint)
    print(ydiscret)
    print(CFDF2)

    assert all(CFDF1 == [0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0])
    assert all(CFDF2 == [0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0])


def test_CFDF_at_a_point_linear_field():
    """
    Test evaluating the CFDF of a linear field.
    """

    x = [0, 2, 3, 4]
    y = [0, 2, 3, 4]
    delta = 1.0

    ydiscret = np.linspace(0.0, 2.0, 11)
    ydiscret3 = np.linspace(0.0, 5.0, 11)

    #
    # Evaluating CFDF at different points
    #
    CFDF1 = CFDFAtPoint(0.0, x, y, delta, ydiscret, boxFilterAtPoint)
    print(ydiscret)
    print(CFDF1)

    CFDF2 = CFDFAtPoint(2.1, x, y, delta, ydiscret, boxFilterAtPoint)
    print(ydiscret)
    print(CFDF2)

    #
    # Test using interval instead of box
    #
    CFDF3 = CFDFAtPoint(2.0, x, y, delta, ydiscret3, intervalFilterAtPoint)
    print(ydiscret3)
    print(CFDF3)

    #
    # Test using interval on the entire array
    #
    CFDF4 = CFDFAtPoint(0.0, x, y, np.inf, ydiscret3, intervalFilterAtPoint)
    print(ydiscret3)
    print(CFDF4)

    assert (
        np.max(
            np.abs(
                CFDF1
                - np.array([0.0, 0.4, 0.8, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0])
            )
        )
        < 1e-5
    )
    assert (
        np.max(
            np.abs(
                CFDF2
                - np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.2, 0.4])
            )
        )
        < 1e-5
    )
    assert (
        np.max(
            np.abs(
                CFDF3
                - np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 1.0, 1.0, 1.0, 1.0, 1.0])
            )
        )
        < 1e-5
    )
    assert (
        np.max(
            np.abs(
                CFDF4
                - np.array(
                    [
                        0.0,
                        1.25e-01,
                        2.50e-01,
                        3.75e-01,
                        5.00e-01,
                        6.25e-01,
                        7.5e-01,
                        8.75e-01,
                        1.0,
                        1.0,
                        1.0,
                    ]
                )
            )
        )
        < 1e-5
    )


def test_FDF_at_a_point_unit_field():
    """
    Test evaluating the FDF of the unit field.
    """

    x = [0, 2, 3, 4]
    y = [1, 1, 1, 1]
    delta = 1.5

    ydiscret = np.linspace(0.0, 2.0, 11)

    #
    # Evaluating CFDF at different points
    #
    FDF1, CFDF1 = FDFAtPoint(0.0, x, y, delta, ydiscret, boxFilterAtPoint)
    print(ydiscret)
    print(FDF1)

    FDF2, CFDF2 = FDFAtPoint(2.1, x, y, delta, ydiscret, boxFilterAtPoint)
    print(ydiscret)
    print(FDF2)

    assert (
        np.max(
            np.abs(
                FDF1 - np.array([0.0, 0.0, 0.0, 0.0, 2.5, 2.5, 0.0, 0.0, 0.0, 0.0, 0.0])
            )
        )
        < 1e-5
    )
    assert (
        np.max(
            np.abs(
                CFDF1
                - np.array([0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0])
            )
        )
        < 1e-5
    )
    assert (
        np.max(
            np.abs(
                FDF2 - np.array([0.0, 0.0, 0.0, 0.0, 2.5, 2.5, 0.0, 0.0, 0.0, 0.0, 0.0])
            )
        )
        < 1e-5
    )
    assert (
        np.max(
            np.abs(
                CFDF2
                - np.array([0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0])
            )
        )
        < 1e-5
    )


def test_FDF_at_a_point_linear_field():
    """
    Test evaluating the FDF of a linear field.
    """

    x = [0, 2, 3, 4]
    y = [0, 2, 3, 4]
    delta = 1.0

    ydiscret = np.linspace(0.0, 2.0, 11)

    #
    # Evaluating CFDF at different points
    #
    FDF1, CFDF1 = FDFAtPoint(0.0, x, y, delta, ydiscret, boxFilterAtPoint)
    print(ydiscret)
    print(FDF1)

    FDF2, CFDF2 = FDFAtPoint(2.1, x, y, delta, ydiscret, boxFilterAtPoint)
    print(ydiscret)
    print(FDF2)

    FDF3, CFDF3 = FDFAtPoint(
        2.1, x, y, delta, ydiscret, boxFilterAtPoint, derivType="forward"
    )
    print(ydiscret)
    print(FDF3)

    FDF4, CFDF4 = FDFAtPoint(
        2.1, x, y, delta, ydiscret, boxFilterAtPoint, derivType="backward"
    )
    print(ydiscret)
    print(FDF4)

    assert (
        np.max(
            np.abs(
                FDF1 - np.array([2.0, 2.0, 1.5, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
            )
        )
        < 1e-5
    )
    assert (
        np.max(
            np.abs(
                CFDF1
                - np.array([0.0, 0.4, 0.8, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0])
            )
        )
        < 1e-5
    )
    assert (
        np.max(
            np.abs(
                FDF2 - np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 1.0, 1.0])
            )
        )
        < 1e-5
    )
    for CFDF in [CFDF2, CFDF3, CFDF4]:
        assert (
            np.max(
                np.abs(
                    CFDF
                    - np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.2, 0.4])
                )
            )
            < 1e-5
        )

    assert (
        np.max(
            np.abs(
                FDF3 - np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0])
            )
        )
        < 1e-5
    )
    assert (
        np.max(
            np.abs(
                FDF4 - np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0])
            )
        )
        < 1e-5
    )


def test_joint_FDF_create():
    """
    Test creating a joint FDF object
    """
    fdf = JointFDFTable()
    fdf.addCoordinate("psi", [0, 5, 10, 15, 20, 25, 30])
    fdf.addCoordinate("rho", [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0])


def test_joint_CFDF_at_a_point_unit_field():
    """
    Test evaluating the joint CFDF of two unit fields.
    """
    #
    # Spatial field
    #
    delta = 0.03
    xmax = 0.1
    xloc = 0.05
    kappa_psi = np.pi / (xmax / 10) / 4.0
    kappa_rho = np.pi / (xmax / 10) / 3.0

    x = np.linspace(0, xmax, 100)
    y = {}
    y["psi"] = 19.0 + 3.0 * np.sin(kappa_psi * x)
    y["rho"] = 0.6 + 0.1 * np.sin(kappa_rho * x)

    #
    # Create FDF object
    #
    fdf = JointFDFTable()
    fdf.addCoordinate("psi", [15, 16, 17, 18, 19, 20, 21, 22, 23])
    fdf.addCoordinate("rho", [0.5, 0.55, 0.6, 0.65, 0.7, 0.75])

    #
    # Evaluating CFDF at one point
    #
    fdf.calculateJointFDF(xloc, x, y, delta, boxFilterAtPoint)

    print(fdf.data["CFDF"])
    print(fdf.data["FDF"])

    assert (np.max(np.abs(fdf.data["CFDF"][4, 2] - 0.6666714))) < 1e-5
    assert (np.max(np.abs(fdf.data["FDF"][4, 1] - 1.443033))) < 1e-5
