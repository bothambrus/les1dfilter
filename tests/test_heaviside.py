#################################################################################
#   LES1DFilter                                                                 #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import numpy as np
from .context import les1dfilter
from les1dfilter.heaviside import heaviside, jointHeaviside


def test_heaviside_linear():
    """
    Test applying applying the Heaviside function on a piecewise linear field.
    """

    x = [
        0,
        2,
        3,
        4,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
    ]
    y = [
        0,
        2,
        3,
        4,
        6,
        7,
        8,
        7,
        6,
        5,
        4,
        3,
        2,
        1,
        2,
        3,
        4,
        5,
    ]
    ylim = 3.6

    #
    # Apply Heaviside function
    #
    xnew, ynew, heavi = heaviside(x, y, ylim)
    print(xnew, ynew)

    #
    # Try with generic joint function
    #
    xnew2, ynew2, heavi2 = jointHeaviside(x, [y], [ylim])
    print(xnew2, ynew2)

    for ix, xi in enumerate(xnew):
        print(xi, ynew[ix], heavi[ix])

    assert len(xnew) == 24
    for h in [heavi, heavi2]:
        assert h[0] == 1
        assert h[3] == 1
        assert h[4] == 0
        assert h[13] == 0
        assert h[14] == 1
        assert h[20] == 1
        assert h[21] == 0
        assert h[23] == 0


def test_heaviside_exact_match():
    """
    Test applying applying the Heaviside function on a piecewise linear field with the limit exactly matching one of the points.
    """

    x = [0, 1, 2, 3, 4, 5]
    y = [5, 4, 3, 2, 1, 0]
    ylim = 3

    #
    # Apply Heaviside function
    #
    xnew, ynew, heavi = heaviside(x, y, ylim)
    print(xnew, ynew)

    for ix, xi in enumerate(xnew):
        print(xi, ynew[ix], heavi[ix])

    assert len(xnew) == 7
    assert all(heavi == [0, 0, 0, 1, 1, 1, 1])


def test_joint_heaviside_linear():
    """
    Test applying applying the joint Heaviside function on two piecewise linear fields.
    """

    x = [
        0,
        2,
        3,
        4,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
    ]
    y = [
        0,
        2,
        3,
        4,
        6,
        7,
        8,
        7,
        6,
        5,
        4,
        3,
        2,
        1,
        2,
        3,
        4,
        5,
    ]
    ylim = 3.6

    z = [
        0,
        2,
        2,
        2,
        2,
        2,
        1,
        1,
        1,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        0,
        0,
    ]
    zlim = 1.4

    #
    # Apply joint  Heaviside function
    #
    xnew, ynews, heavi = jointHeaviside(x, [y, z], [ylim, zlim])
    print(xnew)

    for ix, xi in enumerate(xnew):
        print(
            "{:8.3f} {:8.3f} {:8.3f} {:8.3f}".format(
                xi, ynews[0][ix], ynews[1][ix], heavi[ix]
            )
        )

    assert len(xnew) == 32
    for h in [heavi]:
        assert h[0] == 1
        assert h[1] == 1
        assert h[2] == 0
        assert h[26] == 0
        assert h[27] == 1
        assert h[28] == 1
        assert h[29] == 0

    assert np.sum(heavi) == 4
