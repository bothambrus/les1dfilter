#################################################################################
#   LES1DFilter                                                                 #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import numpy as np
from .context import les1dfilter
from les1dfilter.derivate import centralDerivate, forwardDerivate, backwardDerivate


def test_large_gradient():
    """
    Test gradient of array on sin and cos.
    """
    #
    # On equi-distant
    #
    x = np.linspace(0, 2 * np.pi, 10000)
    y = np.sin(x)

    assert np.max(np.abs(centralDerivate(x, y) - np.cos(x))) < 1e-7
    assert np.max(np.abs(forwardDerivate(x, y) - np.cos(x))) < 1e-3
    assert np.max(np.abs(backwardDerivate(x, y) - np.cos(x))) < 1e-3


def test_edges_gradient():
    """
    Test gradient at edges of array.
    """
    #
    # Test edges on equi-distant
    #
    x = [0, 1, 2, 3, 4]
    y = [0, 2, 3, 2, 0]
    assert all(np.abs(centralDerivate(x, y) - np.array([2, 1.5, 0, -1.5, -2])) < 1e-7)
    assert all(np.abs(forwardDerivate(x, y) - np.array([2, 1.0, -1.0, -2, -2])) < 1e-7)
    assert all(np.abs(backwardDerivate(x, y) - np.array([2, 2, 1, -1, -2])) < 1e-7)


def test_nonuniform_gradient():
    """
    Test gradient at nonuniform grid.
    """
    #
    # Test edges on equi-distant
    #
    x = [0.0, 1.0, 3.0]
    y = [0.0, 2.0, 4.0]

    assert all(
        np.abs(centralDerivate(x, y) - np.array([2.0, 1.6666666666, 1.0])) < 1e-7
    )
    assert all(np.abs(forwardDerivate(x, y) - np.array([2.0, 1.0, 1.0])) < 1e-7)
    assert all(np.abs(backwardDerivate(x, y) - np.array([2.0, 2.0, 1.0])) < 1e-7)


def test_no_gradient():
    """
    Test gradient at of array of 1.
    """
    x = [0]
    y = [1]
    assert all(np.abs(centralDerivate(x, y) - np.array([0])) < 1e-7)
    assert all(np.abs(forwardDerivate(x, y) - np.array([0])) < 1e-7)
    assert all(np.abs(backwardDerivate(x, y) - np.array([0])) < 1e-7)
