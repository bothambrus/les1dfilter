#################################################################################
#   LES1DFilter                                                                 #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import sys
import os
import json

import numpy as np
import matplotlib.pyplot as plt

sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))
from les1dfilter.field import getArrayInRange
from les1dfilter.box import boxFilterAtPoint
from les1dfilter.filterdensity import JointFDFTable
from les1dfilter.integrate import trapezoidal, forwardIntegrate, backwardIntegrate
from les1dfilter.derivate import centralDerivate

from les1dfilter import context
from alyatab.interactive_plotting import getPlotly

names = ["psi", "rho"]
dicnames = ["nsi", "processed"]
keys = ["VELOX", "rho"]
delta = 0.015
# delta = 0.05
xloc = 0.028
npsi = 40

#
# Read data
#
with open("unfilteredData.json", "r") as f:
    sData = json.load(f)

#
# Decide spot
#
iaux = np.argmin(np.abs(np.array(sData["s"]) - xloc))

#
# Part in box
#
yarrs = {}
for ii, name in enumerate(names):
    xarr, yarr = getArrayInRange(
        sData["s"],
        sData[dicnames[ii]][keys[ii]],
        xloc - 0.5 * delta,
        xloc + 0.5 * delta,
    )

    yarrs[name] = yarr


#
# Create FDF object
#
fdf = JointFDFTable()
y = {}

for ii, name in enumerate(names):
    #
    # Assemble coordinates of FDF
    #
    dy = np.max(sData[dicnames[ii]][keys[ii]]) - np.min(sData[dicnames[ii]][keys[ii]])
    ydiscret = np.linspace(
        np.min(sData[dicnames[ii]][keys[ii]]) - 0.05 * dy,
        np.max(sData[dicnames[ii]][keys[ii]]) + 0.05 * dy,
        npsi,
    )

    fdf.addCoordinate(name, ydiscret)

    #
    # Assemble signal
    #
    y[name] = sData[dicnames[ii]][keys[ii]]

#
# Evaluating FDF at one point
#
fdf.calculateJointFDF(xloc, sData["s"], y, delta, boxFilterAtPoint)

#
# Save data
#
fdf.writeText("FDFData_delta{}.dat".format(delta))


#
# Make interactive plot
#
fig = fdf.plot2D_interactive("psi", "rho", verbose=True)
py = getPlotly()
py.offline.plot(fig)
