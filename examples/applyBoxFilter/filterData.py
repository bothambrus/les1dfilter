#################################################################################
#   LES1DFilter                                                                 #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import sys
import os
import json

import numpy as np
import matplotlib.pyplot as plt

sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))
from les1dfilter.box import boxFilter
from les1dfilter.field import getArrayInRange

dicname = "nsi"
key = "VELOX"
delta = 0.01

#
# Read data
#
with open("unfilteredData.json", "r") as f:
    sData = json.load(f)

#
# Create filtered data
#
fData = {}
fData["s"] = sData["s"]
fData["psi"] = list(np.array(sData[dicname][key]))


#
# Apply box filter
#
fData["psi_bar"] = list(boxFilter(fData["s"], fData["psi"], delta))

#
# Double application of box filter
#
fData["psi_bar_bar"] = list(boxFilter(fData["s"], fData["psi_bar"], delta))

#
# Get sub-grid component
#
fData["psi_prime"] = list(np.array(fData["psi"]) - np.array(fData["psi_bar"]))

#
# Apply filter on sub-grid component
#
fData["psi_prime_bar"] = list(boxFilter(fData["s"], fData["psi_prime"], delta))


#
# Write filtered data
#
with open("filteredData_{}_{}_delta{}.json".format(dicname, key, delta), "w") as f:
    json.dump(fData, f, indent=4)


########
# Plot #
########
#
# Figure
#
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

#
# Plot
#
ax.plot(fData["s"], fData["psi"], label="$\psi$")
ax.plot(fData["s"], fData["psi_bar"], label="$\overline{\psi}$")
ax.plot(fData["s"], fData["psi_bar_bar"], label="$\overline{\overline{\psi}}$")
ax.legend()

iaux = np.argmin(np.abs(np.array(fData["s"]) - 0.03))

#
# Part in box
#
xarr, yarr = getArrayInRange(
    fData["s"],
    fData["psi"],
    fData["s"][iaux] - 0.5 * delta,
    fData["s"][iaux] + 0.5 * delta,
)
ax.fill_between(xarr, yarr, color=(0.30, 0.76, 1.00))
ax.plot(xarr, np.ones_like(xarr) * fData["psi_bar"][iaux], color=(0.3, 0.3, 0.3))


#
# Figure
#
fig2 = plt.figure()
ax2 = fig2.add_subplot(1, 1, 1)

#
# Plot
#
ax2.plot(fData["s"], fData["psi_prime"], label="$\psi^{\prime}$")
ax2.plot(fData["s"], fData["psi_prime_bar"], label="$\overline{\psi^{\prime}}$")
ax2.legend()


#
# Figure
#
fig3 = plt.figure()
ax3 = fig3.add_subplot(1, 1, 1)

#
# Plot
#
ax3.plot(fData["s"], fData["psi"], "o", label="$\psi$")
ax3.plot(
    fData["s"],
    np.array(fData["psi_bar_bar"])
    + np.array(fData["psi_prime_bar"])
    + np.array(fData["psi_prime"]),
    label="$\overline{\overline{\psi}} +\overline{\psi^{\prime}} + \psi^{\prime}$",
)
ax3.legend()

plt.show()
