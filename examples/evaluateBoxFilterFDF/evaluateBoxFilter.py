#################################################################################
#   LES1DFilter                                                                 #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import sys
import os
import json

import numpy as np
import matplotlib.pyplot as plt

sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))
from les1dfilter.field import getArrayInRange
from les1dfilter.box import boxFilterAtPoint
from les1dfilter.filterdensity import FDFAtPoint
from les1dfilter.integrate import trapezoidal, forwardIntegrate, backwardIntegrate
from les1dfilter.derivate import centralDerivate

dicname = "nsi"
key = "VELOX"
delta = 0.015
# delta = 0.05
xloc = 0.028
npsi = 100

#
# Read data
#
with open("unfilteredData.json", "r") as f:
    sData = json.load(f)

#
# Decide spot
#
iaux = np.argmin(np.abs(np.array(sData["s"]) - xloc))

#
# Part in box
#
xarr, yarr = getArrayInRange(
    sData["s"],
    sData[dicname][key],
    sData["s"][iaux] - 0.5 * delta,
    sData["s"][iaux] + 0.5 * delta,
)


########
# Plot #
########
#
# Figure
#
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
fig2 = plt.figure()
ax2 = fig2.add_subplot(2, 1, 1)
ax3 = fig2.add_subplot(2, 1, 2)

#
# Plot
#
ax.plot(sData["s"], sData[dicname][key], label="$\psi$")
ax.legend()

ax.fill_between(xarr, yarr, color=(0.30, 0.76, 1.00))


#
# Decide discretization
#
dy = np.max(sData[dicname][key]) - np.min(sData[dicname][key])
ydiscret = np.linspace(
    np.min(sData[dicname][key]) - 0.15 * dy,
    np.max(sData[dicname][key]) + 0.15 * dy,
    npsi,
)


#
# Calculate FDF at a spot
#
for derivType, intType in zip(
    ["forward", "backward", "central"], ["forward", "backward", "trapezoidal"]
):
    FDF, CFDF = FDFAtPoint(
        xloc,
        sData["s"],
        sData[dicname][key],
        delta,
        ydiscret,
        boxFilterAtPoint,
        derivType=derivType,
    )

    fdf_data = dict(
        psi=list(ydiscret),
        FDF=list(FDF),
        CFDF=list(CFDF),
    )

    #
    # Write filtered data
    #
    with open(
        "FDFData_{}_{}_{}_delta{}.json".format(derivType, dicname, key, delta), "w"
    ) as f:
        json.dump(fdf_data, f, indent=4)

    c1 = np.empty_like(ydiscret)
    for ii, y in enumerate(ydiscret):
        if derivType == "forward":
            c1[ii] = forwardIntegrate(ydiscret[: ii + 1], FDF[: ii + 1])
        elif derivType == "backward":
            c1[ii] = backwardIntegrate(ydiscret[: ii + 1], FDF[: ii + 1])
        elif derivType == "central":
            c1[ii] = trapezoidal(ydiscret[: ii + 1], FDF[: ii + 1])

    ########
    # Plot #
    ########
    #
    # Plot CFDF
    #
    ax2.plot(
        ydiscret,
        c1,
        label="repeat integration of $h_\psi$ with {} integration".format(intType),
    )
    if intType == "trapezoidal":
        ax2.plot(
            ydiscret, CFDF, label="$H_\psi$", markersize=3, marker="o", linewidth=0
        )

    #
    # Plot FDF
    #
    ax3.plot(ydiscret, FDF, label="$h_\psi$ {} difference".format(derivType))


#
# Show legends
#
ax2.legend()
ax3.legend()
plt.show()
