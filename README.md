# LES1DFilter

Some scripts for applying filters on 1D fields with Python. 

For details on usage, see the [wiki](https://gitlab.com/bothambrus/les1dfilter/-/wikis/home).
