#################################################################################
#   LES1DFilter                                                                 #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################

import numpy as np

from les1dfilter.derivate import backwardDerivate, centralDerivate, forwardDerivate
from les1dfilter.heaviside import heaviside, jointHeaviside

import les1dfilter.context  # isort:skip
from alyatab.index import i2inds  # isort:skip
from alyatab.table import AlyaTable  # isort:skip


def CFDFAtPoint(x, xvec, yvec, delta, ydiscret, filterFunctionAtPoint):
    """
    Evaluate cumulative filter density function on given `ydiscret` discretization of the field.
    """

    CFDF = np.empty_like(ydiscret)

    for ii, yd in enumerate(ydiscret):
        #
        # Get Heaviside function and corresponding grid, that represent well the jumps
        #
        xnew, ynew, heavi = heaviside(xvec, yvec, yd)

        #
        # Evaluate the filtered value of the Heaviside function
        #
        CFDF[ii] = filterFunctionAtPoint(x, xnew, heavi, delta)

    return CFDF


def FDFAtPoint(
    x, xvec, yvec, delta, ydiscret, filterFunctionAtPoint, derivType="central"
):
    """
    Evaluate filter density function on given `ydiscret` discretization of the field.
    """
    #
    # Evaluate cumulative filter density function
    #
    CFDF = CFDFAtPoint(x, xvec, yvec, delta, ydiscret, filterFunctionAtPoint)

    #
    # Evaluate its marginal counterpart
    #
    if derivType == "central":
        FDF = centralDerivate(ydiscret, CFDF)
    elif derivType == "forward":
        FDF = forwardDerivate(ydiscret, CFDF)
    elif derivType == "backward":
        FDF = backwardDerivate(ydiscret, CFDF)

    return FDF, CFDF


class JointFDFTable(AlyaTable):
    """
    Class to hold joint filter density function data
    """

    def __init__(self, dic={}, keys=[], coordKeys=[], coords={}, typ="single"):
        #
        # Initialize parent class
        #
        AlyaTable.__init__(
            self, dic=dic, keys=keys, coordKeys=coordKeys, coords=coords, typ=typ
        )

    def calculateJointCFDF(self, x, xvec, ydict, delta, filterFunctionAtPoint):
        """
        Calculate the joint Cumulative Filter Density Function.
        The discretization of the random variables is provided by `self.coord`.
        The variable `ydict` is a dictionary with the corresponding signals of random variables.
        `x` provides the location of the filter, while `xvec` is the discretization in space.
        """
        #
        # Add local value of coordinates
        #
        self.addGridOfAllCoordinates()

        #
        # Add variable for CFDF
        #
        self.addEmptyData("CFDF")

        #
        # Assemble yvecs for jointHeaviside
        #
        yvecs = []
        for ick, ck in enumerate(self.coordKeys):
            yvecs.append(ydict[ck])

        #
        # Loop through all points of the table
        #
        for i in range(np.prod(self.shape)):
            #
            # Indeces in table
            #
            ind = i2inds(i, self.shape)

            #
            # Local limiting values
            #
            maxs = []
            for ick, jj in enumerate(ind):
                maxs.append(self.coords[self.coordKeys[ick]][jj])

            #
            # Evaluate joint Heaviside function
            #
            xnew, ynews, heavi = jointHeaviside(xvec, yvecs, maxs)

            #
            # Evaluate the filtered value of the Heaviside function
            #
            self.data["CFDF"][ind] = filterFunctionAtPoint(x, xnew, heavi, delta)

    def calculateJointFDF(
        self, x, xvec, ydict, delta, filterFunctionAtPoint, derivType="central"
    ):
        """
        Calculate the joint Filter Density Function as the partial derivative of the CFDF.
        The discretization of the random variables is provided by `self.coord`.
        The variable `ydict` is a dictionary with the corresponding signals of random variables.
        `x` provides the location of the filter, while `xvec` is the discretization in space.
        """

        #
        # Get CFDF
        #
        self.calculateJointCFDF(x, xvec, ydict, delta, filterFunctionAtPoint)

        #
        # Execute partial derivations
        #
        self.partialDifferentiation(self.coordKeys, "CFDF", "FDF", scheme=derivType)
