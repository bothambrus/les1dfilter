#################################################################################
#   LES1DFilter                                                                 #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import numpy as np


def centralDerivate(x, y):
    """Gets gradient of array over a given grid using central difference.

    Parameters
    ----------
    x : np.ndarray
        The grid to calculate gradient.
    y : np.ndarray
        The array of which the gradinet will be taken.

    Returns
    -------
    np.ndarray 
        The gradient of `y` over `x`.

    Notes
    -----
    In the internal points of the array, a second order discretization is used for non-uniform grid:

    .. math::
        \\frac{d y}{d x}_k = \\ 
        y_{k+1}  * \\frac{h_0}{h_1 * (h_0 + h_1)} \\
        +y_{k}   * \\frac{h_1-h_0}{h_1 * h_0} \\
        +y_{k-1} * \\frac{h_1}{h_0 * (h_0 + h_1)}\\\\

    with:

    .. math::
        h_0 = x_k - x_{k-1}\\\\
        h_1 = x_{k+1} - x_{k}

    The end-points are treated with a first order term:

    .. math::
        \\frac{d y}{d x}_0 = \\ 
        \\frac{y_1-y_0}{x_1-x_0}\\\\
        \\frac{d y}{d x}_{end} = \\ 
        \\frac{y_{end}-y_{end-1}}{x_{end}-x_{end-1}}
        
    """
    #
    # Spetial case of too short array
    #
    if len(x) == 1:
        gradi = np.array([0.0])
        return gradi

    ############
    # Derivate #
    ############
    x = np.array(x, dtype=float)
    y = np.array(y, dtype=float)
    dx = np.diff(x)

    #
    # Internal points
    #
    h0 = dx[:-1]
    h1 = dx[1:]

    gradi = (
        (y[2:] * h0 / (h1 * (h0 + h1)))
        + (y[1:-1] * (h1 - h0) / (h0 * h1))
        - (y[:-2] * h1 / (h0 * (h0 + h1)))
    )

    #
    # Edges
    #
    gradi = np.append([(y[1] - y[0]) / (dx[0])], gradi)
    gradi = np.append(gradi, [(y[-1] - y[-2]) / (dx[-1])])

    return gradi


def forwardDerivate(x, y):
    """Gets gradient of array over a given grid using forward difference where possible.

    Parameters
    ----------
    x : np.ndarray
        The grid to calculate gradient.
    y : np.ndarray
        The array of which the gradinet will be taken.

    Returns
    -------
    np.ndarray
        The gradient of `y` over `x`.

    """
    #
    # Spetial case of too short array
    #
    if len(x) == 1:
        gradi = np.array([0.0])
        return gradi

    ############
    # Derivate #
    ############
    x = np.array(x, dtype=float)
    y = np.array(y, dtype=float)
    dx = np.diff(x)
    dy = np.diff(y)

    #
    # Fist part
    #
    gradi = dy / dx

    #
    # Last point
    #
    gradi = np.append(gradi, [(y[-1] - y[-2]) / (dx[-1])])

    return gradi


def backwardDerivate(x, y):
    """Gets gradient of array over a given grid using backward difference where possible.

    Parameters
    ----------
    x : np.ndarray
        The grid to calculate gradient.
    y : np.ndarray
        The array of which the gradinet will be taken.

    Returns
    -------
    np.ndarray
        The gradient of `y` over `x`.

    """
    #
    # Spetial case of too short array
    #
    if len(x) == 1:
        gradi = np.array([0.0])
        return gradi

    ############
    # Derivate #
    ############
    x = np.array(x, dtype=float)
    y = np.array(y, dtype=float)
    dx = np.diff(x)
    dy = np.diff(y)

    #
    # Fist part
    #
    gradi = dy / dx

    #
    # Last point
    #
    gradi = np.append([(y[1] - y[0]) / (dx[0])], gradi)

    return gradi
