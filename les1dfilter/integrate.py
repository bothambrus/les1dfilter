#################################################################################
#   LES1DFilter                                                                 #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################

import numpy as np


def trapezoidal(x, y):
    """
    Use trapezoidal rule to integrate a given field across its entire length.
    """
    #
    # Make sure they are proper arrays
    #
    xarr = np.array(x, dtype=float)
    yarr = np.array(y, dtype=float)

    #
    # Integrate
    #
    dx = np.diff(xarr)
    inty = np.sum(dx * yarr[:-1])
    inty += np.sum(dx * yarr[1:])
    inty /= 2.0

    return inty


def forwardIntegrate(x, y):
    """
    Use forward integration to integrate a given field across its entire length.
    """
    #
    # Make sure they are proper arrays
    #
    xarr = np.array(x, dtype=float)
    yarr = np.array(y, dtype=float)

    #
    # Integrate
    #
    dx = np.diff(xarr)
    inty = np.sum(dx * yarr[:-1])

    return inty


def backwardIntegrate(x, y):
    """
    Use backward integration to integrate a given field across its entire length.
    """
    #
    # Make sure they are proper arrays
    #
    xarr = np.array(x, dtype=float)
    yarr = np.array(y, dtype=float)

    #
    # Integrate
    #
    dx = np.diff(xarr)
    inty = np.sum(dx * yarr[1:])

    return inty
