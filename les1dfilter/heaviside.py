#################################################################################
#   LES1DFilter                                                                 #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################

import copy

import numpy as np


def heaviside(xvec, yvec, ymax, tol=1e-9):
    """
    Get new array of xvec, and yvec and a binary array of heavi, where y<=ymax
    Assume the original function is piecewise linear.
    Insert new points around the real switchover points, at xswitch +- tol.
    """

    #
    # Find turning points
    #
    highPoints = [ii[0] for ii in np.argwhere(np.array(yvec) <= ymax)]

    #
    # Internal turning points
    #
    turning1 = [highPoints[ii[0]] for ii in np.argwhere(np.diff(highPoints) != 1)]
    turning2 = [
        highPoints[ii[0] + 1] - 1 for ii in np.argwhere(np.diff(highPoints) != 1)
    ]
    turningPoints = turning1 + turning2

    if len(highPoints) > 0 and highPoints[0] != 0:
        #
        # Add first turning point if exists
        #
        turningPoints.append(highPoints[0] - 1)

    if len(highPoints) > 0 and highPoints[-1] != (len(yvec) - 1):
        #
        # Add last turning point if exists
        #
        turningPoints.append(highPoints[-1])

    turningPoints = sorted(turningPoints)

    #
    # Initialize new arrays
    #
    xarr = copy.deepcopy(list(xvec))

    #
    # Add new points around trning points
    #
    for tp in turningPoints:
        #
        # Find real turning point coordinate
        #
        xt = xvec[tp] + (xvec[tp + 1] - xvec[tp]) / (yvec[tp + 1] - yvec[tp]) * (
            ymax - yvec[tp]
        )

        if np.min(np.abs(np.array(xarr) - xt)) < tol:
            #
            # Delete points, that are lower too close to newly added points
            #
            toDelte = sorted(
                [ii[0] for ii in np.argwhere(np.abs(np.array(xarr) - xt) < tol)]
            )[::-1]
            for ii in toDelte:
                del xarr[ii]

        #
        # Add points
        #
        xarr.append(xt + tol)
        xarr.append(xt - tol)

    xarr = sorted(xarr)

    #
    # Interpolate on new coordinates
    #
    correctOrder = np.argsort(xvec)
    yarr = np.interp(xarr, np.array(xvec)[correctOrder], np.array(yvec)[correctOrder])

    #
    # Mark according to criteria
    #
    heavi = np.where(np.interp(xarr, xvec, yvec) <= ymax, 1.0, 0.0)

    return xarr, yarr, heavi


def jointHeaviside(xvec, yvecs, ymaxs, tol=1e-9):
    """
    Get new array of xvec, and array of arrays yvecs and a binary array of heavi, where all(yvecs[i]<=ymaxs[i])
    Assume the original functions are piecewise linear.
    Insert new points around the real switchover points, at xswitch +- tol.
    """
    #
    # Copy inputs as output
    #
    xarr = copy.deepcopy(xvec)
    yarrs = copy.deepcopy(yvecs)
    heaviAll = np.ones_like(xvec, dtype=float)

    #
    # Repeatedly apply heaviside
    # And multiply products
    #
    for iy, (ymax, yarr) in enumerate(zip(ymaxs, yarrs)):
        #
        # Get Heaviside and new grid
        #
        xnew, dummy, heavi = heaviside(xarr, yarr, ymax)

        #
        # Interpolate all fields on new grid
        #
        correctOrder = np.argsort(xarr)
        for ii in range(len(yarrs)):
            yarrs[ii] = np.interp(
                xnew, np.array(xarr)[correctOrder], np.array(yarrs[ii])[correctOrder]
            )
        heaviAll = np.interp(
            xnew, np.array(xarr)[correctOrder], np.array(heaviAll)[correctOrder]
        )

        #
        # Intersect Heavisides
        #
        heaviAll = heavi * heaviAll

        #
        # Propagate
        #
        xarr = xnew

    return xnew, yarrs, heaviAll
