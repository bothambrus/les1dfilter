#################################################################################
#   LES1DFilter                                                                 #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################

import numpy as np

from les1dfilter.field import getArrayInRange
from les1dfilter.integrate import trapezoidal


def intervalFilterAtPoint(xstart, xvec, yvec, delta):
    """
    Apply interval-type filtering of given start point: `xstart` and lenght: `delta` on data.
    """

    y = np.nan

    #
    # Integration limits
    #
    xmin = np.min(xvec)
    xmax = np.max(xvec)

    a = max(xmin, xstart)
    b = min(xmax, xstart + delta)

    #
    # Get limited array in [a,b] interval
    #
    xarr, yarr = getArrayInRange(xvec, yvec, a, b)

    #
    # Integrate
    #
    y = trapezoidal(xarr, yarr) / (xarr[-1] - xarr[0])

    return y
