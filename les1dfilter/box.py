#################################################################################
#   LES1DFilter                                                                 #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################

import numpy as np

from les1dfilter.field import getArrayInRange
from les1dfilter.integrate import trapezoidal


def boxFilterAtPoint(x, xvec, yvec, delta):
    """
    Apply box filtering of given `delta` width on data at one specific point.
    """

    y = np.nan

    #
    # Integration limits
    #
    xmin = np.min(xvec)
    xmax = np.max(xvec)

    a = max(xmin, x - 0.5 * delta)
    b = min(xmax, x + 0.5 * delta)

    #
    # Get limited array in [a,b] interval
    #
    xarr, yarr = getArrayInRange(xvec, yvec, a, b)

    #
    # Integrate
    #
    y = trapezoidal(xarr, yarr) / (xarr[-1] - xarr[0])

    return y


def boxFilter(xvec, yvec, delta):
    """
    Apply box filtering of given `delta` width on data at all points.
    """

    yfilter = np.empty_like(yvec, dtype=float)

    #
    # Apply filter
    #
    for ii, x in enumerate(xvec):
        yfilter[ii] = boxFilterAtPoint(x, xvec, yvec, delta)

    return yfilter
