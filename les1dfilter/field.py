#################################################################################
#   LES1DFilter                                                                 #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################

import copy

import numpy as np


def getArrayInRange(xvec, yvec, a, b):
    """
    Get new array of xvec, and yvec in the x: [a,b] range
    Interpolate on the interval limits
    """
    #
    # Find INTERNAL indeces
    #
    imin = 0
    for ii in range(len(xvec) - 1):
        if xvec[ii] <= a and a < xvec[ii + 1]:
            imin = ii + 1

    imax = 0
    for ii in range(1, len(xvec)):
        if xvec[ii - 1] < b and b <= xvec[ii]:
            imax = ii - 1

    #
    # Weights of boundary indeces
    #
    wa = float(a - xvec[imin - 1]) / float(xvec[imin] - xvec[imin - 1])
    wb = float(b - xvec[imax]) / float(xvec[imax + 1] - xvec[imax])

    #
    # Assemble array
    #
    xarr = copy.deepcopy(list(xvec[imin : imax + 1]))
    yarr = copy.deepcopy(list(yvec[imin : imax + 1]))

    #
    # Add limiting values
    #
    ya = yvec[imin - 1] + wa * (yvec[imin] - yvec[imin - 1])
    yb = yvec[imax] + wb * (yvec[imax + 1] - yvec[imax])

    xarr.insert(0, a)
    xarr.append(b)
    yarr.insert(0, ya)
    yarr.append(yb)

    xarr = np.array(xarr)
    yarr = np.array(yarr)

    return xarr, yarr
